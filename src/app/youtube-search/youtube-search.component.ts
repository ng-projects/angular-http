import { Component, OnInit } from '@angular/core';
import { YoutubeSearchResult } from './youtube-search-result';

@Component({
  selector: 'app-youtube-search',
  templateUrl: './youtube-search.component.html',
  styleUrls: ['./youtube-search.component.css']
})
export class YoutubeSearchComponent implements OnInit {
  results: YoutubeSearchResult[];
  loading: boolean;

  constructor() { }

  updateResults(results: YoutubeSearchResult[]): void {
    this.results = results;
    console.log("results", this.results);
  }

  ngOnInit() {
  }

}
