import { Component, Input, OnInit } from "@angular/core";
import { YoutubeSearchResult } from "./youtube-search-result";

@Component({
    selector: "app-search-result",
    templateUrl: "./youtube-search-result.component.html"
})

export class YoutubeResultComponent implements OnInit{
    @Input() result: YoutubeSearchResult;

    constructor() {}

    ngOnInit() {

    }
}